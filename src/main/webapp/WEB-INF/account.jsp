<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link href="<c:url value="/css/jquery-ui-tabs.css" />" rel="stylesheet" >
	<link href="<c:url value="/css/normalize.css" />" rel="stylesheet">
	<link href="<c:url value="/css/user.css" />" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>conturi</title>
</head>
<body>
   <div id="tabs">
  <ul>
    <li><a href="#Clients" id="tabC">Clients</a></li>
    <li><a href="#Accounts" id="tabA">Accounts</a></li>
  </ul>
  <div class = "toolbar">
  	<select id="accountOptions">
  		<option value="noop"></option>
  		<option value="Add">Add Account</option>
  		<option value="Delete">Delete Account</option>
  		<option value="Edit">Edit Account</option>
  		<option value="Transfer">Transfer </option>
  		<option value="ViewC">View All Clients</option>
  		<option value="ViewA">View All Accounts</option>
  		<option value="Pay">Pay A Bill</option>
  	</select>
  	  <input type="submit" class="btnLook" id="logout" value="Log out">
  </div>
  <div id="Clients">
  </div>
  <div id="Accounts">
  	<div id="view">
  		<h1>List of All Accounts</h1>
	    <table>
			<tr>
			  <th>ID</th>
			  <th>Type</th> 
			  <th>Amount</th>
			  <th>Choose Account</th>
			</tr>
	        <c:forEach var="a" items="${accounts}">
			<tr>
			  <td class="id">${a.aid}</td>
			  <td>${a.type}</td> 
			  <td class="amount">${a.amount}</td>
			  <td><input type="checkbox" name='element' id="statusAct" value="1" /></td>
			</tr>
	        </c:forEach>
		</table>
  	</div>
  	<div id="add" class="action hidden">
  		<p><u><b>Add Account form</b></u></p>
  		<div>Type:	<input type="text" name="type"><br></div>
	    <div>Amount:<input type="number" name="amount" min="1" max="10000" step="2"><br></div>
	    <input id="addAccount" type="submit" class="btnLook" value="Submit">
  	</div>
  	<div id="delete" class="action hidden">
		<br><label><u><b>Attention! The selected account will be permanently deleted.</b></u></label><br><br>
		<input id="deleteAccount" type="submit" class="btnLook" value="Delete">
	</div>
	<div id="edit" class="action hidden">
		<p><u><b>Edit Account form</b></u></p>
		<div>Type:<input type="text" name="edittype"><br></div>
	    <div>Amount:	<input type="number" min="1" max="10000" step="2" name="editamount"><br></div>
		<input id="editAccount" type="submit" class="btnLook" value="Edit">
	</div>
	<div id="transfer" class="action hidden">
		<p><u><b>Transfer between accounts</b></u></p>
		<input type="text" name="from">
		<img alt="Direction" src="/img/right_arrow.png">
		<input type="text" name="to">
		<div>Amount:<input type="number" min="1" max="10000" step="2" name="transferamount"><br></div>
		<input id="transferAmount" type="submit" class="btnLook" value="Transfer">
	</div>
	<div id="pay" class="action hidden">
		<p><u><b>Pay Bill</b></u></p>
		<div>Bill Name:<input type="text" name="editbillName"><br></div>
		<div>Amount :<input type="text" name="editBillAmount"><br></div>
		<input id="payBill" type="submit" class="btnLook" value="Pay">
	</div>
  </div>
</div>

	<script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.js"></script>
	<script type="text/javascript" src="/js/user.js"></script>
	<script type="text/javascript" src="/js/account.js"></script>
	<script type="text/javascript" src="/js/login.js"></script>
</body>
</html>