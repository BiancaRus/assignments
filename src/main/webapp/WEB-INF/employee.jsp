<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link href="<c:url value="/css/jquery-ui-tabs.css" />" rel="stylesheet" >
	<link href="<c:url value="/css/normalize.css" />" rel="stylesheet">
	<link href="<c:url value="/css/user.css" />" rel="stylesheet">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Employees</title>
</head>
<body>
 <div class = "toolbar">
  	<select id="employeeOptions">
  		<option value=""></option>
  		<option value="View">View All Employees</option>
  		<option value="Add">Add Employee</option>
  		<option value="Delete">Delete Employee</option>
  		<option value="Edit">Edit Employee</option>
  		<option value="Report">Generate Report</option>
  	</select>
  	<input type="submit" class="btnLook" id="logout" value="Log out">
  </div>
  <div id="Employees">
  	<div id="view">
   		<h1>List of All Employees</h1>
	    <table>
			<tr>
			  <th>ID</th>
			  <th>Name</th> 
			  <th>Gender</th>
			  <th>CNP</th>
			  <th>Choose Employee</th>
			</tr>
	        <c:forEach var="e" items="${employees}">
			<tr>
			  <td class="id">${e.id}</td>
			  <td class="name">${e.name}</td>
			  <td class="gender"><c:choose>
			      <c:when test="${e.gender=='true'}">Female</c:when>
			      <c:otherwise>Male</c:otherwise>
			  </c:choose></td>
			  <td class="cnp">${e.cnp}</td>
			  <td><input type="radio" name='element' id="statusAct" value="1" /></td>
			</tr>
	        </c:forEach>
		</table>
	</div>
	<div id="add" class="action hidden">
		<p><u><b>Add Employee form</b></u></p>
		<div>User name*:<input type="text" name="username"><br></div>
	    <div>CNP*:	<input type="text" name="cnp"><br></div>
	    <div>Gender: <select id="gender"><option value="false">Male</option><option value="true">Female</option></select></div>
	    <input id="addEmployee" class="btnLook" type="submit" value="Submit">
	</div>
	<div id="delete" class="action hidden">
		<br><label><u><b>Attention! The selected user will be permanently deleted.</b></u></label><br><br>
		<input id="deleteEmployee" class="btnLook" type="submit" value="Delete">
	</div>
	<div id="edit" class="action hidden">
		<p><u><b>Edit Client form</b></u></p>
		<div>User name:<input type="text" name="editusername"><br></div>
	    <div>CNP:	<input type="text" name="editcnp"><br></div>
	    <div>Gender: <select id="editgender"><option value="false">Male</option><option value="true">Female</option></select></div>
		<input id="editEmployee" class="btnLook" type="submit" value="Edit">
	</div>
	<div id="report" class="action hidden">
		<p><u><b>Generate Report</b></u></p>
	    <div>From:	<input type="text" class="datepicker" readonly='true' name="fromdate"></div>
	    <div>To: 	<input type="text" class="datepicker" readonly='true' name="todate"></div>
		<input id="genReport" class="btnLook" type="submit" value="Generate Report">
	</div>
  </div>
	<script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.js"></script>
	<script type="text/javascript" src="/js/employee.js"></script>
	<script type="text/javascript" src="/js/login.js"></script>
</body>
</html>