<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link href="<c:url value="/css/jquery-ui-tabs.css" />" rel="stylesheet" >
	<link href="<c:url value="/css/normalize.css" />" rel="stylesheet">
	<link href="<c:url value="/css/user.css" />" rel="stylesheet">
	<link href="<c:url value="/css/login.css" />" rel="stylesheet">
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>LOGIN</title>
</head>
<body>
	<div id="container">
	<fieldset>
		<legend>Login form</legend>
		<form:form commandName="loginForm" id="loginForm" modelAttribute="loginForm">
			<ul>
				<li>
					<label>Username:</label>
					<span class="fieldHolder">
					<form:input path="name" cssErrorClass="errorField" />
					<form:errors path="name" cssClass="error" /></span>
				</li>
				<li>
					<label>Password:</label>
					<span class="fieldHolder">
						<form:password path="password" cssErrorClass="errorField"/>
						<form:errors path="password" cssClass="error" />
					</span>
				</li>
				<li>
					<label>&nbsp;</label>
					<span class="fieldHolder">
						<input id="login" type="submit" class="btnLook" title="Submit" value="LOGIN" />
					</span>
				</li>
			</ul>
		</form:form>
	</fieldset>
	</div>
		<script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
		<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
		<script type="text/javascript" src="/js/jquery.validate.min.js"></script>
		<script type="text/javascript" src="/js/login.js"></script>
</body>
</html>