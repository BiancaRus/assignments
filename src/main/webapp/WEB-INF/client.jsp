<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link href="<c:url value="/css/jquery-ui-tabs.css" />" rel="stylesheet" >
	<link href="<c:url value="/css/normalize.css" />" rel="stylesheet">
	<link href="<c:url value="/css/user.css" />" rel="stylesheet">
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>clienti</title>
</head>
<body>
<div id="tabs">
  <ul>
    <li><a href="#Clients" id="tabC">Clients</a></li>
    <li><a href="#Accounts" id="tabA">Accounts</a></li>
  </ul>
  <div class = "toolbar">
  	<select id="clientOptions">
  		<option value="noop"></option>
  		<option value="View">View All Clients</option>
  		<option value="Add">Add Client</option>
  		<option value="Delete">Delete Client</option>
  		<option value="Edit">Edit Client</option>
  		<option value="AddAccount">Add Account</option>
  	</select>
  <input type="submit" class="btnLook" id="logout" value="Log out">
  </div>
  <div id="Clients">
  	<div id="view">
   		<h1>List of All Clients</h1>
	    <table>
			<tr>
			  <th>ID</th>
			  <th>Name</th> 
			  <th>Address</th>
			  <th>CNP</th>
			  <th>Choose User</th>
			</tr>
	        <c:forEach var="c" items="${clients}">
			<tr>
			  <td>${c.pid}</td>
			  <td class="name">${c.name}</td> 
			  <td class="address">${c.address}</td>
			  <td class="cnp">${c.cnp}</td>
			  <td><input type="radio" name='element' id="statusAct" value="1" /></td>
			</tr>
	        </c:forEach>
		</table>
	</div>
	<div id="add" class="action hidden">
		<p><u><b>Add Client form</b></u></p>
		<div>User name*:<input type="text" name="username"><br></div>
	    <div>CNP*:	<input type="text" name="cnp"><br></div>
	    <div>Address:<input type="text" name="address"><br></div>
	    <div>Gender: <select id="gender"><option value="false">Male</option><option value="true">Female</option></select></div>
	    <input id="addClient" type="submit" class="btnLook" value="Submit">
	</div>
	<div id="delete" class="action hidden">
		<br><label><u><b>Attention! The selected user will be permanently deleted.</b></u></label><br><br>
		<input id="deleteClient" type="submit" class="btnLook" value="Delete">
	</div>
	<div id="edit" class="action hidden">
		<p><u><b>Edit Client form</b></u></p>
		<div>User name:<input type="text" name="editusername"><br></div>
	    <div>CNP:	<input type="text" name="editcnp"><br></div>
	    <div>Address:<input type="text" name="editaddress"><br></div>
	    <div>Gender: <select id="editgender"><option value="false">Male</option><option value="true">Female</option></select></div>
		<input id="editClient" type="submit" class="btnLook" value="Edit">
	</div>
  </div>
  <div id="Accounts">
  	<div id="view">
  		<h1>List of All Accounts</h1>
	    <table>
			<tr>
			  <th>ID</th>
			  <th>Type</th> 
			  <th>Amount</th>
			  <th>Choose Account</th>
			</tr>
	        <c:forEach var="a" items="${accounts}">
			<tr>
			  <td>${a.aid}</td>
			  <td>${a.type}</td> 
			  <td>${a.amount}</td>
			  <td><input type="radio" name='element' id="statusAct" value="1" /></td>
			</tr>
	        </c:forEach>
		</table>
  	</div>
  </div>
</div>
	<script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.js"></script>
	<script type="text/javascript" src="/js/user.js"></script>
	<script type="text/javascript" src="/js/account.js"></script>
	<script type="text/javascript" src="/js/login.js"></script>
</body>
</html>