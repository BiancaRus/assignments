package service;

import java.util.Date;
import java.util.List;

import modelDAO.EmployeeDAO;

import org.springframework.beans.factory.annotation.Autowired;

import views.LoginForm;
import entity.Employee;
import entity.Transaction;

public class EmployeeService {

	@Autowired private EmployeeDAO employeeDAO;
	@Autowired private TransactionService transactionService;
	
	public List<Employee> getAllEmployees(){
		List<Employee> employees = employeeDAO.findAll();
		return employees;
	}
	
	public boolean isAdmin(LoginForm employee){
		Employee employeeDB = employeeDAO.getEmployee(employee);
		return employeeDB.isAdmin();
	}
	
	public Employee whoIsOnline(){
		Employee employeeDB = employeeDAO.getLoggedInEmployee();
		return employeeDB;
	}
	
	public Employee getEmployee(LoginForm employee){
		return employeeDAO.getEmployee(employee);
	}

	public void addEmployee(Employee employee) {
		employeeDAO.addEmployee(employee);
	}
	
	public void deleteEmployee(int id){
		employeeDAO.deleteEmployee(id);
	}
	
	public void editEmployee(Employee employee){
		if(!employee.getName().isEmpty()){
			employeeDAO.editName(employee);
		}
		if(employee.getCnp() > 0){
			employeeDAO.editCNP(employee);
		}
		employeeDAO.editGender(employee);
	}
	
	public void setWhoIsOnline(Employee employee){
		employeeDAO.editOnlineness(employee);
	}
	
	public void logOut(){
		Employee e = this.whoIsOnline();
		employeeDAO.logOutUser(e);
	}
	
	/** modify to get all transactions performed by the employee e!!!!*/
	//@SuppressWarnings("unused")
	/*public void generateReports(Employee e, Date date1, Date date2){
		List<Transaction> transactions = transactionService.getAllTransactions();
		ReportService servRep = new ReportService(transactions);
	}
	*/
}
