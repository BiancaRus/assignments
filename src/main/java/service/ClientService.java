package service;

import java.util.List;

import modelDAO.ClientDAO;

import org.springframework.beans.factory.annotation.Autowired;

import entity.Account;
import entity.Client;

public class ClientService {

	@Autowired private ClientDAO clientDAO;
	@Autowired private AccountService accountService;
	
	public List<Client> findAll(){
		List<Client> clients = clientDAO.findAll();
		return clients;
	}
	
	public void addClient(Client client){
		clientDAO.addClient(client);
	}
	
	public void deleteClient(int clientID){
		clientDAO.deleteClient(clientID);
	}
	
	public void editClient(Client client){
		clientDAO.updateClientLongField(client.getPid(), client.getCnp());
		  clientDAO.updateClientBooleanField(client.getPid(), client.getGender());
		  clientDAO.updateClientStringField("name", client.getPid(), client.getName());
		  clientDAO.updateClientStringField("address", client.getPid(), client.getAddress());
	}
	
	public Client getClientByAccountID(int aid){
		Account account = (Account) accountService.getAccountByID(aid);
		int cid = account.getPid().getPid();
		Client client = this.getClientByClientID(cid);
		return client;
	}
	
	public Client getClientByClientID(int cid){
		Client client = clientDAO.getClientByID(cid);
		return client;
	}
}
