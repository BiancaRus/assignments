package service;

import java.util.List;

import modelDAO.AccountDAO;

import org.springframework.beans.factory.annotation.Autowired;

import entity.Account;
import entity.Client;

public class AccountService {

	@Autowired private AccountDAO accountDAO;
	@Autowired private TransactionService transactionService;
	@Autowired private ClientService serviceClient;
	
	public List<Account> listAccounts() {
		List<Account> accounts = accountDAO.findAll();
		return accounts;
	  }
	
	public List<Account> getAccountsByClientID(int id) {
		List<Account> accounts = accountDAO.findAllByClientID(id);
		return accounts;
	  }
	
	public Account getAccountByID(int id){
		Account account = accountDAO.getAccountByAccountID(id);
		return account;
	}
	
	public String deleteAccount(Integer accountId) {
		accountDAO.deleteAccount(accountId);
		return "account";
	  }
	
	public void addAccount(Account account){
		accountDAO.addAccount(account);
	}
	
	public void editAccount(Account account){
		accountDAO.updateAccountIntegerField(account.getAid(), account.getAmount());
		accountDAO.updateAccountStringField(account.getAid(), account.getType());
	}
	
	public String transferBetweenAccounts(Integer fromId, Integer toId, Integer amount){
		Client fromClient = serviceClient.getClientByAccountID(fromId);
		Client toClient = serviceClient.getClientByAccountID(toId);
		Account fromAccount = (Account) this.getAccountByID(fromId);
		Account toAccount = (Account) this.getAccountByID(toId);
	  	int initialVal = fromAccount.getAmount();
	  	int initialVal2 = toAccount.getAmount();
	  	int newValue = initialVal - amount;
	  	if(newValue < 0){
	  		return new String("NotEnoughMoney");
	  	} else {
		  	accountDAO.transferBetweenAccounts(fromAccount, toAccount, initialVal - amount, initialVal2 + amount);
			/* also add a transaction object representing this transfer */
			if(fromClient.getPid() == toClient.getPid()){
				transactionService.addTransaction(fromClient.getPid(), "transfer");
			} else {
				transactionService.addTransaction(fromClient.getPid(), "transfer");
				transactionService.addTransaction(toClient.getPid(), "transfer");
			}
	  	}
	  	return "Success";
	}
	
	public void payABill(Integer aid, Integer amount, Integer pid){
		Account account = this.getAccountByID(aid);
		int initValue = account.getAmount();
		accountDAO.payABill(account, initValue-amount);
		transactionService.addTransaction(pid, "bill");
	}
}
