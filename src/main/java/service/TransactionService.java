package service;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;

import modelDAO.TransactionDAO;

import org.springframework.beans.factory.annotation.Autowired;

import entity.Client;
import entity.Employee;
import entity.Transaction;

public class TransactionService {

	@Autowired private TransactionDAO transactionDAO;
	@Autowired private EmployeeService employeeService;
	
	public List<Transaction> getAllTransactions(){
		List<Transaction> transactions = transactionDAO.findAll();
		return transactions;
	}
	
	public void addTransaction(Integer pid, String type){
		Transaction tr = new Transaction();
		tr.setCid(new Client(pid));
		tr.setType(type);
		//fetch logged-in employee from DB
		Employee em = employeeService.whoIsOnline();
		tr.setEid(em);
		Calendar cal = Calendar.getInstance();
		Date date = new Date(cal.getTimeInMillis());
		tr.setDate(date);
		transactionDAO.addTransaction(tr);
	}
}
