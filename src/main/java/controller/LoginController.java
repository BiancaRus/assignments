package controller;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import service.EmployeeService;
import validators.LoginFormValidator;
import views.LoginForm;
import entity.Employee;

@Controller
@SuppressWarnings({"unchecked", "rawtypes"})
@RequestMapping("/")
public class LoginController {
	@Autowired private LoginFormValidator validator;
	@Autowired private EmployeeService serviceEmployee;
	@Autowired private LoginForm loginForm;
	
	@InitBinder private void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
	 }
	
	@RequestMapping(value = "login", method = RequestMethod.GET)
	public String showForm(Map model) throws Exception {
        model.put("loginForm", loginForm);//creates new object (with bean)
        return "index";//returns .jsp file
	}
	
	@RequestMapping(value="login", method = RequestMethod.POST)
	  public String login(Model model, @Valid @ModelAttribute LoginForm loginForm, BindingResult result) {
		if (result.hasErrors()) {
            return "index";
        }
		Employee employee = serviceEmployee.getEmployee(loginForm);
		boolean adminFlag = employee.isAdmin();
		if(adminFlag) {
			serviceEmployee.setWhoIsOnline(employee);
			return "redirect:/employee";
		} else {
			serviceEmployee.setWhoIsOnline(employee);
			return "redirect:/client";
		}	 
	}
	
	@RequestMapping(value = "logout", method = RequestMethod.GET)
	public String logout(Map model) throws Exception {
        model.put("loginForm", loginForm);//creates new object (with bean)
        serviceEmployee.logOut();
        return "redirect:/login";//returns .jsp file
	}
}
