package controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import service.EmployeeService;
import entity.Employee;

@Controller
//@SuppressWarnings({"unchecked", "rawtypes"})
@RequestMapping("/")
public class EmployeeController {
	@Autowired private EmployeeService serviceEmployee;
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }
	
	@RequestMapping(value="employee", method = RequestMethod.GET)
	  public String listEmployees(Model model) {
		List<Employee> employees = serviceEmployee.getAllEmployees();
	    model.addAttribute("employees", employees);
	    return "employee";
	  }
	
	@RequestMapping(value="add-employee", method = RequestMethod.POST)
	  public String addEmployee(Model model, @RequestBody Employee employee) {
		serviceEmployee.addEmployee(employee);
		List<Employee> employees = serviceEmployee.getAllEmployees();
		model.addAttribute("employees", employees);
		return "employee";
	  }
	
	@RequestMapping(value="delete-employee/{employeeId}", method = RequestMethod.DELETE)
	  public String deleteEmployee(@PathVariable Integer employeeId) {
		serviceEmployee.deleteEmployee(employeeId);
		return "employee";
	  }
	
	@RequestMapping(value="edit-employee", method = RequestMethod.POST)
	  public String editEmployee(@RequestBody Employee employee) {
		serviceEmployee.editEmployee(employee);
		return "employee";
	  }
	
	//@RequestMapping(value="report-employee/{employeeId}/{date1}/{date2}", method = RequestMethod.GET)
	  /*public String generateReports(@PathVariable("employeeId") int eid, @PathVariable("date1") Date date1, @PathVariable("date2") Date date2) {
		Employee employee = serviceEmployee.whoIsOnline();
		if(employee.isAdmin()){
			serviceEmployee.generateReports(employee, date1, date2);
		} 
		return "employee";
	  }
	  */
}
