package controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import service.ClientService;
import entity.Client;

@Controller
@RequestMapping("/")
public class ClientController {
   
  @Autowired private ClientService clientService;
   
  /**
   * This handler method is invoked when
   * http://localhost:8080/bankapp is requested.
   * The method returns view name "index"
   * which will be resolved into /WEB-INF/index.jsp.
   *  See src/main/webapp/WEB-INF/servlet-context.xml
   */
  @RequestMapping(value="client", method = RequestMethod.GET)
  public String listClients(Model model) {
    List<Client> clients = clientService.findAll();
    model.addAttribute("clients", clients);
    return "client";
  }
  
  @RequestMapping(value="add-client", method = RequestMethod.POST)
  public String addClient(@RequestBody Client client) {
	  clientService.addClient(client);
	  return "client";
  }
  
  @RequestMapping(value="delete-client/{clientId}", method = RequestMethod.GET)
  public String deleteClient(@PathVariable Integer clientId) {
	  clientService.deleteClient(clientId);
	  return "client";
  }
  
  @RequestMapping(value="edit-client", method = RequestMethod.POST)
  public String editClient(@RequestBody Client client) {
	  clientService.editClient(client);
	  return "client";
  }
  
}