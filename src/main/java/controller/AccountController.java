package controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import service.AccountService;
import service.EmployeeService;
import service.TransactionService;
import entity.Account;

@Controller
@RequestMapping("/")
public class AccountController {
	
	@Autowired private AccountService accountService;
	@Autowired private TransactionService transactionService;
	@Autowired private EmployeeService serviceEmployee;
	
	@RequestMapping(value="accounts", method = RequestMethod.GET)
	  public String listAccounts(Model model) {
		List<Account> accounts = accountService.listAccounts();
		model.addAttribute("accounts", accounts);
		return "account";
	  }
	
	@RequestMapping(value="accounts/{id}", method = RequestMethod.GET)
	public String listAccountsByClientID(Model model, @PathVariable int id) {
		List<Account> accounts = accountService.getAccountsByClientID(id);
		model.addAttribute("accounts", accounts);
		return "account";
	}
	
	  @RequestMapping(value="add-account", method = RequestMethod.POST)
	  public String addAccount(@RequestBody Account account) {
		  accountService.addAccount(account);
		  return "account";
	  }
	
	  @RequestMapping(value="delete-account/{accountId}", method = RequestMethod.GET)
	  public String deleteAccount(@PathVariable Integer accountId) {
		  accountService.deleteAccount(accountId);
		  return "account";
	  }
	  
	  @RequestMapping(value="edit-account", method = RequestMethod.POST)
	  public String editAccount(@RequestBody Account account) {
		  accountService.editAccount(account);
		  return "account";
	  }
	  
	  @RequestMapping(value="transfer/{fromId}/{toId}/{amount}", method = RequestMethod.GET)
	  public String transfer(@PathVariable Integer fromId, @PathVariable Integer toId, @PathVariable Integer amount) {
		  String status = accountService.transferBetweenAccounts(fromId, toId, amount);
		  if(status.equals("NotEnoughMoney")){
			  return "account";
		  }
		  return "account";
	  }
	  
	  @RequestMapping(value="pay/{aid}/{amount}/{pid}", method = RequestMethod.GET)
	  public String payBill(@PathVariable Integer aid, @PathVariable Integer amount, @PathVariable Integer pid) {
		  accountService.payABill(pid, amount, aid);
		  return "account";
	  }
}
