package modelDAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import entity.Client;


@Repository
@SuppressWarnings({"unchecked", "rawtypes"})
public class ClientDAO {
  @Autowired private SessionFactory sessionFactory;
   
  /**
   * @Transactional annotation below will trigger Spring Hibernate transaction manager to automatically create
   * a hibernate session. See src/main/webapp/WEB-INF/servlet-context.xml
   */
  @Transactional
  public List<Client> findAll() {
    Session session = sessionFactory.getCurrentSession();
    List clients = session.createQuery("from Client").list();
    return clients;
  }
  
  @Transactional
  public void addClient(Client client) {
	  Session session = sessionFactory.getCurrentSession();
	  session.save(client);
  }
  
  @Transactional
  public Client getClientByID(int id){
	  Session session = sessionFactory.getCurrentSession();
	  Client client = (Client) session.get(Client.class, id);
	  return client;
  }
  
  @Transactional
  public void updateClientStringField(String fieldName, int id, String value){
	  Session session = sessionFactory.getCurrentSession();
	  Object daoClient = session.get(Client.class, id);
	  Client client = (Client) daoClient;
	  client.setField(fieldName, value);
  }
  
  @Transactional
  public void updateClientLongField(int id, long value){
	  Session session = sessionFactory.getCurrentSession();
	  Object daoClient = session.get(Client.class, id);
	  Client client = (Client) daoClient;
	  client.setCnp(value);
  }
  
  @Transactional
  public void updateClientBooleanField(int id, Boolean value){
	  Session session = sessionFactory.getCurrentSession();
	  Object objClient = session.get(Client.class, id);
	  Client client = (Client) objClient;
	  client.setGender(value);
  }
  
  @Transactional
  public void deleteClient(int id){
	  Session session = sessionFactory.getCurrentSession();
	  Object objClient = session.get(Client.class, id);
	  session.delete(objClient);
  }
  
}