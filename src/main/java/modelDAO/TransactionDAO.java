package modelDAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import entity.Transaction;

@Repository
@SuppressWarnings({"unchecked", "rawtypes"})
public class TransactionDAO {

	@Autowired private SessionFactory sessionFactory;
	
	@Transactional
	public List<Transaction> findAll(){
		Session session = sessionFactory.getCurrentSession();
	    List transactions = session.createQuery("from Transaction").list();
	    return transactions;
	}
	
	@Transactional
	public void addTransaction(Transaction transaction){
		Session session = sessionFactory.getCurrentSession();
		session.save(transaction);
	}
}
