package modelDAO;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import entity.Account;

@Repository
@SuppressWarnings({"unchecked"})
public class AccountDAO {
  @Autowired private SessionFactory sessionFactory;
   
  /**
   * @Transactional annotation below will trigger Spring Hibernate transaction manager to automatically create
   * a hibernate session. See src/main/webapp/WEB-INF/servlet-context.xml
   */
  @Transactional
  public List<Account> findAll() {
    Session session = sessionFactory.getCurrentSession();
    List<Account> accounts = session.createQuery("from Account").list();
    return accounts;
  }
  
  @Transactional
  public List<Account> findAllByClientID(int id) {
	Session session = sessionFactory.getCurrentSession();
	List<Account> list = session.createQuery("from Account").list();
	List<Account> listaacc = new ArrayList<Account>();
	for(Account a: list){
		if(a.getPid().getPid() == id){
			listaacc.add(a);
		}
	}
	return listaacc;
  }
  
  @Transactional
  public Account getAccountByAccountID(int id) {
	  Session session = sessionFactory.getCurrentSession();
	  Object objAccount = session.get(Account.class, id);
	  Account account = (Account) objAccount;
	  return account;
  }
  
  @Transactional
  public void addAccount(Account account) {
	  Session session = sessionFactory.getCurrentSession();
	  session.save(account);
  }
  
  @Transactional
  public void updateAccountStringField(int id, String value){
	  Session session = sessionFactory.getCurrentSession();
	  Object daoAccount = session.get(Account.class, id);
	  Account account = (Account) daoAccount;
	  account.setType(value);
  }
  
  @Transactional
  public void updateAccountIntegerField(int id, Integer value){
	  Session session = sessionFactory.getCurrentSession();
	  Object objAccount = session.get(Account.class, id);
	  Account account = (Account) objAccount;
	  account.setAmount(value);
  }
  
  @Transactional
  public void deleteAccount(int id){
	  Session session = sessionFactory.getCurrentSession();
	  Object objAccount = session.get(Account.class, id);
	  session.delete(objAccount);
  }
  
  @Transactional
  @SuppressWarnings("unused")
  public void transferBetweenAccounts(Account from, Account to, int newFromAmount, int newToAmount){
	  Session session = sessionFactory.getCurrentSession();
	  from.setAmount(newFromAmount);
	  to.setAmount(newToAmount);
  }
  
  @SuppressWarnings("unused")
  @Transactional
  public void payABill(Account payingAccount, int value) {
	  Session session = sessionFactory.getCurrentSession();
	  payingAccount.setAmount(value);
  }
}