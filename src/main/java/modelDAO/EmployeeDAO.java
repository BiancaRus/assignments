package modelDAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import views.LoginForm;
import entity.Employee;

@Repository
@SuppressWarnings({"unchecked", "rawtypes"})
public class EmployeeDAO {

	@Autowired private SessionFactory sessionFactory;
	
	@Transactional
	public List<Employee> findAll(){
		Session session = sessionFactory.getCurrentSession();
	    List employees = session.createQuery("from Employee").list();
	    return employees;
	}
	
	  @Transactional
	  public Employee getEmployee(LoginForm employee){
		Session session = sessionFactory.getCurrentSession();
		String username = employee.getName();
		String password = employee.getPassword();
		Employee aUser = (Employee) session.createCriteria(Employee.class).add(Restrictions.and(Restrictions.eq("name",username),
		Restrictions.eq("password", password))).uniqueResult();
		return aUser;
	  }
	  
	  @Transactional
	  public Employee getLoggedInEmployee(){
		Session session = sessionFactory.getCurrentSession();
		Employee aUser = (Employee) session.createCriteria(Employee.class).add(Restrictions.eq("isLoggedIn",true)).uniqueResult();
		return aUser;
	  }

	  @Transactional
	  public void addEmployee(Employee employee){
		  Session session = sessionFactory.getCurrentSession();
		  session.save(employee);
	  }
	  
	  @Transactional
	  public void deleteEmployee(int id){
		  Session session = sessionFactory.getCurrentSession();
		  Object objEmployee= session.get(Employee.class, id);
		  session.delete(objEmployee);
	  }
	  
	  @Transactional
	  public void editName(Employee employee){
		  Session session = sessionFactory.getCurrentSession();
		  Object daoObj = session.get(Employee.class, employee.getId());
		  Employee employeeO = (Employee) daoObj;
		  employeeO.setName(employee.getName());
	  }
	  
	  @Transactional
	  public void editCNP(Employee employee){
		  Session session = sessionFactory.getCurrentSession();
		  Object daoObj = session.get(Employee.class, employee.getId());
		  Employee employeeO = (Employee) daoObj;
		  employeeO.setCnp(employee.getCnp());
	  }
	  
	  @Transactional
	  public void editGender(Employee employee){
		  Session session = sessionFactory.getCurrentSession();
		  Object daoObj = session.get(Employee.class, employee.getId());
		  Employee employeeO = (Employee) daoObj;
		  employeeO.setGender(employee.isGender());
	  }
	  
	  @Transactional
	  public void editOnlineness(Employee employee){
		  Session session = sessionFactory.getCurrentSession();
		  Object daoObj = session.get(Employee.class, employee.getId());
		  Employee employeeO = (Employee) daoObj;
		  employeeO.setLoggedIn(true);
	  }
	  
	  @Transactional
	  public void logOutUser(Employee employee){
		  Session session = sessionFactory.getCurrentSession();
		  Object daoObj = session.get(Employee.class, employee.getId());
		  Employee employeeO = (Employee) daoObj;
		  employeeO.setLoggedIn(false);
	  }
}

