package validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import views.LoginForm;

@Component
public class LoginFormValidator implements Validator{
	public boolean supports(Class<?> clazz) {
		return LoginForm.class.equals(clazz);
	}

	public void validate(Object loginForm, Errors errors) {
		LoginForm logForm = (LoginForm) loginForm; 
		
		if (logForm.getPassword().isEmpty()){
			errors.rejectValue("password", "password_cannot_be_empty");
		}
		if (logForm.getName().isEmpty()){
			errors.rejectValue("password", "username_cannot_be_empty");
		}
		if (logForm.getPassword().length() < 6){
			errors.rejectValue("password", "password_too_short");
		}
	}
}
