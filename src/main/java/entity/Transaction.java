package entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name = "transaction")
public class Transaction {
	@Id
	@GeneratedValue
	private int id;
	private Date date;
	@ManyToOne(targetEntity = Client.class)
    @JoinColumn(name = "cid")
	private Client cid;
	@ManyToOne(targetEntity = Employee.class)
    @JoinColumn(name = "eid")
	private Employee eid;
	private String type;
	
	public Transaction(){}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Client getCid() {
		return cid;
	}
	public void setCid(Client cid) {
		this.cid = cid;
	}
	public Employee getEid() {
		return eid;
	}
	public void setEid(Employee eid) {
		this.eid = eid;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
}
