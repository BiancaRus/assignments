package entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name = "account")
public class Account {

	@Id
	@GeneratedValue
	private int aid;
	private String type;
	private Integer amount;
	@ManyToOne(targetEntity = Client.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "pid", referencedColumnName="pid")
	private Client pid;
	
	public Account(){}
	
	public Account(String type, Integer amount, int pid) {
		this.type = type;
		this.amount = amount;
		this.pid = new Client(pid);
	}

	public Client getPid() {
		return pid;
	}
	public void setPid(Client id){
		this.pid = id;
	}
	public int getAid() {
		return aid;
	}
	public void setAid(int aid) {
		this.aid = aid;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	
}
