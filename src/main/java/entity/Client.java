package entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "client")
public class Client {
	@Id
	@GeneratedValue
	@Column(name="pid")
	private int pid;
	private String name;
	private Long cnp;
	private String address;
	private Boolean gender;
	
	public Client(){}
	public Client(int pid){this.pid = pid;}

	public Client(String name, long cnp, String address, Boolean gender) {
		this.name = name;
		this.cnp = cnp;
		this.address = address;
		this.gender = gender;
	}

	public long getCnp() {
		return cnp;
	}

	public void setCnp(long cnp) {
		this.cnp = cnp;
	}

	public Boolean getGender() {
		return gender;
	}

	public void setGender(Boolean gender) {
		this.gender = gender;
	}

	public int getPid() {
		return this.pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setField(String field, String value){
		if(field.equals("name")){
			this.setName(value);
		} else if(field.equals("address")){
			this.setAddress(value);
		} 
	}

}