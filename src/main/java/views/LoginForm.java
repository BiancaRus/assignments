package views;

import javax.validation.constraints.*;

public class LoginForm {
	@NotNull
	@Size(min = 1)  
	private String name;
	@NotNull
	@Size(min = 1)  
	private String password;
	
	public String getName() {
		return name;
	}
	public void setName(String userName) {
		this.name = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
